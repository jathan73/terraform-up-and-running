terraform {
 required_version = ">= 0.8.8"
}

provider "aws" {
  region = "eu-west-1"
}

resource "aws_instance" "terraform-ec2-1" {
  ami                    = "ami-09c60c18b634a5e00"
  instance_type          = "t2.micro"
  vpc_security_group_ids = ["${aws_security_group.instance.id}"]

user_data = <<-EOF
            #!/bin/bash
            echo "Hallo Welt" > index.html
            nohup busybox httpd -f -p 8080 &
            EOF

  tags = {
   Name = "terraform-ec2-1"
  }
}

resource "aws_security_group" "instance" {
  name = "terraform-ec2-1-sg"

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

output "public_ip" {
  value = "${aws_instance.terraform-ec2-1.public_ip}"
}
